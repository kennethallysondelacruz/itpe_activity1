import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';
import HomeScreen from './screens/HomeScreen'
import DetailsScreen from './screens/DetailsScreen'
import ActivityIndicatorScreen from './screens/ActivityIndicatorScreen'
import ButtonScreen from './screens/ButtonScreen';
import ImageScreen from './screens/ImageScreen';
import ModalScreen from './screens/ModalScreen';
import PickerScreen from './screens/PickerScreen';
import ProgressBarScreen from './screens/ProgressBarScreen';
import KeyboardAvoidingViewScreen from './screens/KeyBoardAvoidingViewScreen';
import SliderScreen from './screens/SliderScreen';
import RefreshControlScreen from './screens/RefreshControlScreen';


const RootStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    Details: {
      screen: DetailsScreen,
    },
    Activity: {
      screen: ActivityIndicatorScreen,
    },
    Button: {
      screen: ButtonScreen,
    },
    Image: {
      screen: ImageScreen,
    },
    Keyboard: {
      screen: KeyboardAvoidingViewScreen,
    },
    Modal: {
      screen: ModalScreen,
    },
    Picker: {
      screen: PickerScreen,
    },
    Progress: {
      screen: ProgressBarScreen,
    },
    Slider: {
      screen: SliderScreen,
    },
    Refresh: {
      screen: RefreshControlScreen,
    },
  },
  {
    initialRouteName: 'Home',
  }
);

export default class App extends Component {
  render() {
    return <RootStack />;
  }
}